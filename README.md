This test has been developed for YapAiTek company!
# Main Notebook
* [This Notebook](../notebooks/eda.ipynb) contains all steps that are 
developed to estimate the final market share.
# Dependencies
* To install dependencies feel free to create a virtual environment and after activatation, run _pip install -r requirements.txt_. Then you can run existing code or develop your own ideas.
# Outputs
* You can find the test.csv (with estimated scores) and the regressioner model in _ouputs_ directory.
